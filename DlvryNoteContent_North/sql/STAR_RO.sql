/******************************************************************************
NAME: STAR_RO.rpt
SUBREPORT: -
DESC: STAR interface

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created
1.1			11.05.2023		PBRUGGER	 IMI-3703		removed HU01, HU02, HU03

******************************************************************************/

WITH 
 CL AS
    (select companywh.whid,
        companywh.company_id,
        party.party_id,
        party.name1,
        party.name2,
        party.name3,
        party.name4,
        party.name5,
        party.adr1,
        party.adr2,
        party.postcode,
        party.city,
        party.countrycode,
        party.country,
        party.vat_number,
        company.type_of_goods
    from party party
        join companywh companywh 
         on companywh.company_id = party.party_id
        join company company
         on company.company_id = companywh.COMPANY_ID
    where party.party_qualifier = 'CL'),
 CU AS
    (select companywh.whid,
        companywh.company_id,
        party.party_id,
        party.name1,
        party.name2,
        party.name3,
        party.name4,
        party.name5,
        party.adr1,
        party.adr2,
        party.postcode,
        party.city,
        party.countrycode,
        party.country,
        partycontact.phone,
        partycontact.telex
    from party party
        join partycontact partycontact
         on partycontact.party_id = party.party_id
         and partycontact.company_id = party.company_id
        join companywh companywh 
         on companywh.company_id = party.company_id
    where party.party_qualifier = 'CU'),
 WH AS
    (select companywh.whid,
        companywh.company_id,
        party.party_id,
        party.name3,
        party.name4,
        party.name5,
        party.adr1,
        party.adr2,
        party.postcode,
        party.city,
        party.countrycode,
        party.country
    from party party
        join companywh companywh 
         on companywh.whid = party.party_id
    where party.party_qualifier = 'WH'),
 VOL AS
    (SELECT SUM(CASE WHEN CAR.CARTYPID = 'CAR'
                THEN 0.096
                ELSE CAR.TOTVOL
            END) AS VOL,
    NVL(PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID) CARID,
    COUNT(CAR.CARID) NUM_BOXES
    FROM CAR
    JOIN PBCAR
        ON PBCAR.CARID = CAR.CARID
        AND CAR.CARTYPID != 'EUR'
        AND CAR.CARTYPID != 'HPL'
    GROUP BY NVL(PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID))
SELECT
    ---------
    -- 01000
    ---------
    TO_CHAR(SYSDATE, 'YYYYMMDD') "DATE", --01001, 02001AB, 02001AH, 02001EM, 02001R1, 03001, 13001
    'DAP' "INCOTERMS", --01003, 01005
    CONS.CONSIGNMENT_ID "CONSIGNMENT_ID", 
    CONS.FRE_CONSIGNMENT_ID "FRE_CONSIGNMENT_ID", --01007
    VOL.VOL "VOLUME", --01008
    SUM(CASE
        WHEN (CAR.USER_LOADING_METER IS NULL) THEN (CAR.LOADING_METER) 
        WHEN (CAR.USER_LOADING_METER IS NOT NULL) THEN (CAR.USER_LOADING_METER)
        ELSE 0
    END) "LOADING_METER", --01009
    TO_CHAR (PBCAR.LPSDTM, 'YYYYMMDD') "END_CUSTOMER_DATE", --01027
    null "FIX_DELIVERY_DATE_2", --01028
    'NO' "FIX_DELIVERY_DATE", --01050 (YES -> "FIX")
    ---------
    -- 02000
    ---------
    --AB/AH
    CL.NAME3 "AB_AH_NAME1", --02005ABADR01, 02005AHADR01
    CL.NAME4 "AB_AH_NAME2", --02005ABADR02, 02005AHADR02
    WH.ADR1 "AB_AH_ADR1", --02005ABADR03, 02005AHADR03
    WH.ADR2 "AB_AH_ADR2", --02005ABADR03, 02005AHADR03
    WH.COUNTRYCODE "AB_AH_COUNTRYCODE", --02005ABADR04, 02005AHADR04
    WH.POSTCODE "AB_AH_POSTCODE", --02005ABADR05, 02005AHADR05
    WH.CITY "AB_AH_CITY", --02005ABADR06, 02005AHADR06
    --EM
    CU.PARTY_ID "EM_PARTYID", --02005EMADR01
    CU.NAME1 "EM_NAME1", --02005EMADR01
    CU.NAME2 "EM_NAME2", --02005EMADR02
    CU.ADR1 "EM_ADR1", --02005EMADR03
    CU.ADR2 "EM_ADR2", --02005EMADR03
    CU.COUNTRYCODE "EM_COUNTRYCODE", --01012 (J, N), 02005EMADR04
    CU.POSTCODE "EM_POSTCODE", --02005EMADR05
    CU.CITY "EM_CITY", --02005EMADR06
    CU.PHONE "EM_PHONE", --04006VA/VB/VD
    CU.TELEX "SHIP_DATE", --04006VA/VB/VD
    DEP.LOADFINISHDTM "SHIP_DATE_ALT", --04006VA/VB/VD alternative
    --R1
    CL.VAT_NUMBER "R1_VAT_NUMBER", --02005R1
    CL.NAME3 "R1_NAME1", --02005R1ADR01
    CL.NAME4 "R1_NAME2", --02005R1ADR02
    CL.ADR1 "R1_ADR1", --02005R1ADR03
    CL.ADR2 "R1_ADR2", --02005R1ADR03
    CL.COUNTRYCODE "R1_COUNTRYCODE", --02005R1ADR04
    CL.POSTCODE "R1_POSTCODE", --02005R1ADR05
    CL.CITY "R1_CITY", --02005R1ADR06
    CONS.TERMS_OF_DELIVERY_LOCATION "INCOTERMS_LOCATION",
    ---------
    -- 03000
    ---------
    PBCAR.CARID, --03005, 13003
    REPLACE(CARTYP.CARTYPID2, '4.5', 'BX') CARTYPID2, --03006
    CARTYP.CARTYPID, --03006
    CL.TYPE_OF_GOODS, --03009
    VOL.NUM_BOXES, --03009
    ---------
    -- NO_PREFIX
    ---------
    PBCAR.PIKNOPAKS,
    CEIL(PBCAR.PIKWEIGHT),
    PBCAR.MERGE_TO_CARID
FROM CONSIGNMENT CONS
    JOIN DEP DEP
     ON DEP.DEPARTURE_ID = CONS.DEPARTURE_ID
	JOIN PBCAR PBCAR 
	 ON PBCAR.CONSIGNMENT_ID = CONS.CONSIGNMENT_ID
	  AND PBCAR.PBCARSTAT IN('C','L')
	  AND PBCAR.CONSOLIDATION_TO_CARID IS NULL
	/*JOIN PBROW PBROW
	 ON PBROW.PBCARID = PBCAR.PBCARID*/
	JOIN CAR CAR
	 ON CAR.CARID = PBCAR.CARID
	  AND CAR.COMPANY_ID = PBCAR.COMPANY_ID
	JOIN CARTYP CARTYP
	 ON CARTYP.CARTYPID = CAR.CARTYPID
    /*JOIN PAK PAK
	 ON PAK.COMPANY_ID = PBROW.COMPANY_ID
	  AND PAK.ARTID = PBROW.ARTID
	  AND PAK.PAKID = PBROW.PAKID*/
	JOIN CL
     ON CL.whid = cons.whid
      AND CL.company_id = cons.company_id
      AND CL.party_id = cons.company_id
	JOIN CU
     ON CU.whid = cons.whid
      AND CU.company_id = cons.company_id
      AND CU.party_id = cons.shiptopartyid
	JOIN WH
     ON WH.whid = cons.whid
      AND WH.company_id = cons.company_id
      AND WH.party_id = cons.whid
    LEFT JOIN VOL
     ON VOL.CARID = CAR.CARID
WHERE CONS.COMPANY_ID IN ('RO-LCWOFF', 'RO-LCWON')
 AND CONS.CONSIGNMENT_ID = '{?CONSIGNMENT_ID}'
 AND cons.shiptopartyid NOT IN  ('RO99', 'PL02', 'PL03', 'PL04')
GROUP BY 
    ---------
    -- 01000
    ---------
    TO_CHAR(SYSDATE, 'YYYYMMDD'), --01001, 02001AB, 02001AH, 02001EM, 02001R1
    'DAP', --01003, 01005
    CONS.CONSIGNMENT_ID,
    CONS.FRE_CONSIGNMENT_ID, --01007
    VOL.VOL, --01008
    TO_CHAR (PBCAR.LPSDTM, 'YYYYMMDD'), --01027
    null, --01028
    'NO', --01050 (YES -> "FIX")
    ---------
    -- 02000
    ---------
    --AB/AH
    CL.NAME3, --02005ABADR01, 02005AHADR01
    CL.NAME4, --02005ABADR02, 02005AHADR02
    WH.ADR1, --02005ABADR03, 02005AHADR03
    WH.ADR2, --02005ABADR03, 02005AHADR03
    WH.COUNTRYCODE, --02005ABADR04, 02005AHADR04
    WH.POSTCODE, --02005ABADR05, 02005AHADR05
    WH.CITY, --02005ABADR06, 02005AHADR06
    --EM
    CU.PARTY_ID, --02005EMADR01
    CU.NAME1, --02005EMADR01
    CU.NAME2, --02005EMADR02
    CU.ADR1, --02005EMADR03
    CU.ADR2, --02005EMADR03
    CU.COUNTRYCODE, --01012 (J, N), 02005EMADR04
    CU.POSTCODE, --02005EMADR05
    CU.CITY, --02005EMADR06
    CU.PHONE, --04006VA/VB/VD
    CU.TELEX, --04006VA/VB/VD
    DEP.LOADFINISHDTM, --04006VA/VB/VD alternative
    --R1
    CL.VAT_NUMBER, --02005R1
    CL.NAME3, --02005R1ADR01
    CL.NAME4, --02005R1ADR02
    CL.ADR1, --02005R1ADR03
    CL.ADR2, --02005R1ADR03
    CL.COUNTRYCODE, --02005R1ADR04
    CL.POSTCODE, --02005R1ADR05
    CL.CITY, --02005R1ADR06
    CONS.TERMS_OF_DELIVERY_LOCATION,
    ---------
    -- 03000
    ---------
    PBCAR.CARID, --03005, 13003
    REPLACE(CARTYP.CARTYPID2, '4.5', 'BX'), --03006
    CARTYP.CARTYPID, --03006
    CL.TYPE_OF_GOODS, --03009
    VOL.NUM_BOXES, --03009
    ---------
    -- NO_PREFIX
    ---------
    PBCAR.PIKNOPAKS,
    CEIL(PBCAR.PIKWEIGHT),
    PBCAR.MERGE_TO_CARID