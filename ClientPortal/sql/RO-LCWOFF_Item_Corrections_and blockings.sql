/******************************************************************************
NAME: RO-LCWOFF_Customer_Orders_sent.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

SELECT 
    ITECHG.ITEID, 
    ITECHG.ARTID, 
    ITECHG.STORQTY, 
    ITECHG.STORQTY2,
    ITECHG.BLOCKCOD,
    (SELECT BLOCKCODNM FROM BLOCKCOD WHERE BLOCKCOD.BLOCKCOD = ITECHG.BLOCKCOD) BLOCKTXT,
    ITECHG.BLOCKCOD2, 
    (SELECT BLOCKCODNM FROM BLOCKCOD WHERE BLOCKCOD.BLOCKCOD = ITECHG.BLOCKCOD2) BLOCKTXT1,
    ITECHG.ITECHGCODID, 
    ITECHG.EMPID, 
    ITECHG.SENDSTAT, 
    ITECHG.INSERTDTM DTM,
    ITECHGCOD.ITECHGCODNAME,
    ITECHG.POID,
    ITECHG.ITECHGTXT
FROM ITECHG
JOIN ITECHGCOD
    ON ITECHG.ITECHGCODID=ITECHGCOD.ITECHGCODID
WHERE COMPANY_ID='RO-LCWOFF'
ORDER BY ITECHG.INSERTDTM DESC


