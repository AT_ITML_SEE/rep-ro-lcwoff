/******************************************************************************
NAME: RO-LCWOFF_Free_locations.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

SELECT wp.wsid,
         WP.WPTYPE,
         WP.OPSTAT,
         WP.MAXSPCE,
         WP.WHID,
         wp.wpadr,
         NVL (to_char(SPFRE.FRESPCE), 0) FRE_SPACE,
         COUNT (DISTINCT car.carid) NO_LC,
         COUNT (DISTINCT DELCAR.CARID) RESV_LC
    FROM wp
         LEFT OUTER JOIN spfre
            ON     spfre.whid = wp.whid
               AND spfre.wsid = wp.wsid
               AND spfre.wpadr = wp.wpadr
         LEFT OUTER JOIN car
            ON     car.wsid = wp.wsid
               AND car.whid = wp.whid
               AND car.wpadr = wp.wpadr
         LEFT OUTER JOIN delcar
            ON     DELCAR.TOWPADR = wp.wpadr
               AND delcar.towsid = wp.wsid
               AND DELCAR.COMPANY_ID = 'RO-LCWOFF'
   WHERE wp.whid = 'BUK1' AND wptype <> 'P' AND wp.wpadr like 'A%'
GROUP BY wp.wsid,
         WP.WPTYPE,
         WP.OPSTAT,
         WP.MAXSPCE,
         WP.WHID,
         wp.wpadr,
         NVL (to_char(SPFRE.FRESPCE), 0)
UNION ALL
  SELECT wp.wsid,
         WP.WPTYPE,
         '1',
         WP.MAXSPCE,
         WP.WHID,
         wp.wpadr,
         NVL (pp.wpadr, 1) FRE_SPACE,
         COUNT (DISTINCT car.carid) NO_LC,
         COUNT (DISTINCT DELCAR.CARID) RESV_LC
    FROM wp
         LEFT OUTER JOIN pp
            ON     pp.whid = wp.whid
               AND pp.wsid = wp.wsid
               AND pp.wpadr = wp.wpadr
         LEFT OUTER JOIN car
            ON     car.wsid = wp.wsid
               AND car.whid = wp.whid
               AND car.wpadr = wp.wpadr
         LEFT OUTER JOIN delcar
            ON     DELCAR.TOWPADR = wp.wpadr
               AND delcar.towsid = wp.wsid
               AND DELCAR.COMPANY_ID = 'RO-LCWOFF'
   WHERE wp.whid = 'BUK1' AND wptype = 'P' AND wp.wpadr like 'A%'
GROUP BY wp.wsid,
         WP.WPTYPE,
         WP.OPSTAT,
         WP.MAXSPCE,
         WP.WHID,
         wp.wpadr,
         NVL (pp.wpadr, 1)