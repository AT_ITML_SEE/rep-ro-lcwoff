 /******************************************************************************
NAME: RO-LCWOFF_Transport_report.rpt
SUBREPORT: 
DESC: Transport report
    
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
--------------------------------------------------------------------------------
1.0 		??.??.???? 		????????            	    created
1.1 		02.05.2022 		DAKORNHA     IMI-2116       Refactoring/Indexing

******************************************************************************/

SELECT DEP.DEPARTURE_ID,
    CASE DEP.DEPARTURE_PICKSTAT 
        WHEN 'O' THEN 'Open'
        WHEN 'R' THEN 'Ready'
        WHEN 'H' THEN 'Hold'
        WHEN 'F' THEN 'Finished'
        WHEN 'C' THEN 'Not started'
    END DEPPICKSTAT,
    DEP.DEPARTURE_DTM,
    DEP.LOADFINISHDTM,
    CO.COID,
    SUBSTR(CO.CO_REF,-4) CO_REF,
    CO.INSERTDTM,
    COWORK.SHIPDTM DELIVERYDATE,
    PBCAR.CONSIGNMENT_ID,
    SUM(CASE WHEN CAR.CARTYPID = 'CAR'
                THEN 0.096
                ELSE CAR.TOTVOL
            END) VOL,
    COUNT(NVL(PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID)) NUM_CAR,
    SUM(DECODE(CAR.CARTYPID, 'CAR', 20, PBCAR.WEIGHT)) WEIGHT,
    PARTY.PARTY_ID,
    PARTY.NAME1,
    PARTY.ADR1,
    PARTY.ADR2,
    PARTY.POSTCODE,
    PARTY.CITY,
    PARTY.COUNTRYCODE,
    PARTYCONTACT.PHONE,
    'DB Schenker' PICKUPFROM , 
    '087015' PICKUPZIPCODE,
    'Bucharest' PICKUPCITY,
    'CTP Bucharest WEST 2' PICKUPADRESS
FROM COWORK
JOIN PARTY
    ON PARTY.PARTY_ID = COWORK.SHIPTOPARTYID
    AND PARTY.COMPANY_ID = COWORK.COMPANY_ID
    AND PARTY.PARTY_QUALIFIER = COWORK.SHIPTOPARTYQUALIFIER
LEFT JOIN PARTYCONTACT 
    ON PARTYCONTACT.PARTY_ID = PARTY.PARTY_ID
    AND PARTYCONTACT.PARTY_QUALIFIER = PARTY.PARTY_QUALIFIER
    AND PARTYCONTACT.COMPANY_ID = 'RO-LCWOFF'    
JOIN CO
    ON CO.COID = COWORK.COID
    AND CO.COSEQ = COWORK.COSEQ
    AND CO.COMPANY_ID = COWORK.COMPANY_ID
LEFT JOIN DEP
    ON DEP.DEPARTURE_ID = COWORK.DEPARTURE_ID
    AND DEP.WHID = 'BUK1' 
JOIN PBCAR
    ON PBCAR.DEPARTURE_ID = DEP.DEPARTURE_ID    
    AND PBCAR.COMPANY_ID = 'RO-LCWOFF'
    AND PBCAR.WHID = 'BUK1'
JOIN CAR
    ON CAR.CARID = PBCAR.CARID
    AND CAR.COMPANY_ID = COWORK.COMPANY_ID
    AND CAR.WHID = COWORK.WHID
WHERE COWORK.COMPANY_ID = 'RO-LCWOFF'
    AND DEP.DEPARTURE_DTM
        BETWEEN {?DEP_DATE_FROM}
        AND {?DEP_DATE_TO}
--        BETWEEN TO_DATE('01.04.2022', 'DD.MM.YYYY')
--        AND TO_DATE('30.04.2022', 'DD.MM.YYYY')
GROUP BY DEP.DEPARTURE_ID,
    DEP.DEPARTURE_PICKSTAT ,
    DEP.DEPARTURE_DTM,
    DEP.LOADFINISHDTM,
    CO.COID,
    SUBSTR(CO.CO_REF,-4),
    CO.INSERTDTM,
    COWORK.SHIPDTM,
    PBCAR.CONSIGNMENT_ID,
    PARTY.PARTY_ID,
    PARTY.NAME1,
    PARTY.ADR1,
    PARTY.ADR2,
    PARTY.POSTCODE,
    PARTY.CITY,
    PARTY.COUNTRYCODE,
    PARTYCONTACT.PHONE
UNION
SELECT DEP.DEPARTURE_ID,
    'Finished' DEPPICKSTAT,
    DEP.DEPARTURE_DTM,
    DEP.LOADFINISHDTM,
    CO.COID,
    SUBSTR(CO.CO_REF,-4) CO_REF,
    CO.INSERTDTM,
    CO.SHIPDTM DELIVERYDATE,
    PBCAR.CONSIGNMENT_ID,
    SUM(CASE WHEN CAR.CARTYPID = 'CAR'
                THEN 0.096
                ELSE CAR.TOTVOL
            END) VOL,
    COUNT(NVL(PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID)) NUM_CAR,
    SUM(DECODE(CAR.CARTYPID, 'CAR', 20, PBCAR.WEIGHT)) WEIGHT,
    PARTY.PARTY_ID,
    PARTY.NAME1,
    PARTY.ADR1,
    PARTY.ADR2,
    PARTY.POSTCODE,
    PARTY.CITY,
    PARTY.COUNTRYCODE,
    PARTYCONTACT.PHONE,
    'DB Schenker' PICKUPFROM , 
    '087015' PICKUPZIPCODE,
    'Bucharest' PICKUPCITY,
    'CTP Bucharest WEST 2' PICKUPADRESS
FROM COTRC CO
JOIN PARTY
    ON PARTY.PARTY_ID = CO.SHIPTOPARTYID
    AND PARTY.COMPANY_ID = CO.COMPANY_ID
LEFT JOIN PARTYCONTACT 
    ON PARTYCONTACT.PARTY_ID = PARTY.PARTY_ID
    AND PARTYCONTACT.PARTY_QUALIFIER = PARTY.PARTY_QUALIFIER
    AND PARTYCONTACT.COMPANY_ID = 'RO-LCWOFF'   
JOIN CARTRC CAR
    ON CAR.COID = CO.COID
    AND CAR.COSEQ = CO.COSEQ
    AND CAR.DEPARTURE_ID = CO.DEPARTURE_ID
    AND CAR.WHID = CO.WHID 
LEFT JOIN DEPLOG DEP
    ON DEP.DEPARTURE_ID = CO.DEPARTURE_ID
    AND DEP.WHID = 'BUK1' 
JOIN PBCARLOG PBCAR
    ON PBCAR.DEPARTURE_ID = DEP.DEPARTURE_ID  
    AND PBCAR.CARID = CAR.CARID
    AND PBCAR.COMPANY_ID = 'RO-LCWOFF'
    AND PBCAR.WHID = 'BUK1'
WHERE CO.COMPANY_ID = 'RO-LCWOFF'
    AND CO.WHID = 'BUK1'
    AND DEP.DEPARTURE_DTM
        BETWEEN {?DEP_DATE_FROM}
        AND {?DEP_DATE_TO}
--        BETWEEN TO_DATE('01.04.2022', 'DD.MM.YYYY')
--        AND TO_DATE('30.04.2022', 'DD.MM.YYYY')
GROUP BY DEP.DEPARTURE_ID,
    DEP.DEPARTURE_DTM,
    DEP.LOADFINISHDTM,
    CO.COID,
    SUBSTR(CO.CO_REF,-4),
    CO.INSERTDTM,
    CO.SHIPDTM,
    PBCAR.CONSIGNMENT_ID,
    PARTY.PARTY_ID,
    PARTY.NAME1,
    PARTY.ADR1,
    PARTY.ADR2,
    PARTY.POSTCODE,
    PARTY.CITY,
    PARTY.COUNTRYCODE,
    PARTYCONTACT.PHONE
ORDER BY COID DESC,
    CONSIGNMENT_ID DESC,
    DEPARTURE_DTM DESC


