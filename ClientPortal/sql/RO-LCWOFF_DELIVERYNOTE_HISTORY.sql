/******************************************************************************
NAME: RO-LCWOFF_DELIVERYNOTE_HISTORY.rpt
SUBREPORT: 
DESC: Delivery note RO-LCWOFF
    
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
--------------------------------------------------------------------------------
1.0 		??.??.???? 		????????            	    created
1.1 		15.04.2022 		DAKORNHA     IMI-2116       Refactoring/Indexing

******************************************************************************/

SELECT DISTINCT
    PARTY.PARTY_ID,
    PARTY.NAME1,
    PARTY.ADR1,
    PARTY.ADR2,
    PARTY.PHONE,
    PARTY.POSTCODE,
    PARTY.CITY,
    PARTY.COUNTRYCODE,
    CO.COID,
    PBCAR.CONSIGNMENT_ID,
    PBROW.FPSHIPDTM,
    SUBSTR(CO.CO_REF, -4),
    SUBSTR(CO.CO_REF, -2),
    ART.ARTID,
    ART.ARTNAME2,
    PAK.PAKID,
    PAK.PAKNAME,
    CAR.CARTYPID AS CARTYPID,
    CAR.CARTYPID AS CARTYP,
    PBCAR.CONSOLIDATION_TO_CARID AS CONSOLIDATION_TO_CARID,
    PBCAR.CONSOLIDATION_TO_CARID AS ONLY_CONS_ID,
    CONSIGNMENT.FRE_CONSIGNMENT_ID,
    PBROW.COPOS,
    CO.DEPARTURE_ID,
    SUM (PBROW.PICKQTY),
    DECODE (PAK.PAKID, 'CAR', 20, PAK.WEIGHT) AS PIKWEIGHT,
    CO.COSEQ,
    CO.COSUBSEQ,
    COUNT (DISTINCT DECODE(CAR.CARTYPID, 'EUR', NVL (PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID), null))
       OVER (PARTITION BY CAR.CARTYPID, PBCAR.CONSIGNMENT_ID) AS EUR,
    COUNT (DISTINCT DECODE(CAR.CARTYPID, 'EUR', null, NVL (PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID)))
       OVER (PARTITION BY CAR.CARTYPID, PBCAR.CONSIGNMENT_ID) AS NON_EUR,
    SUM(CASE WHEN CAR.CARTYPID = 'CAR'
            THEN 0.096
            ELSE CAR.TOTVOL
        END) 
        OVER (PARTITION BY PBCAR.CONSIGNMENT_ID) AS VOL
FROM COTRC CO
LEFT JOIN CONSIGNMENTTRC CONSIGNMENT
    ON CO.CONSIGNMENT_ID = CONSIGNMENT.CONSIGNMENT_ID
JOIN PARTY
    ON PARTY.PARTY_ID = CO.SHIPTOPARTYID
    AND PARTY.PARTY_QUALIFIER = CO.SHIPTOPARTYQUALIFIER
    --AND PARTY.NAME1 = CO.SHIPTOPARTY_NAME1
    AND PARTY.COMPANY_ID = CO.COMPANY_ID
    AND PARTY.PARTY_QUALIFIER = 'CU'
JOIN PBROWLOG PBROW
    ON PBROW.COID = CO.COID
    AND PBROW.WHID = CO.WHID
    AND PBROW.COSEQ = CO.COSEQ
    AND PBROW.COSUBSEQ = CO.COSUBSEQ
JOIN PBCARLOG PBCAR
    ON PBCAR.PBCARID = PBROW.PBCARID
JOIN CARTRC CAR
    ON CAR.CARID =  NVL(PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID)
    AND CAR.COMPANY_ID =  CO.COMPANY_ID
JOIN ART
    ON ART.ARTID = PBROW.ARTID
    AND ART.COMPANY_ID = PBROW.COMPANY_ID
JOIN PAK
    ON PAK.ARTID = ART.ARTID
    AND PAK.COMPANY_ID = ART.COMPANY_ID
WHERE CO.WHID = 'BUK1'
    AND CO.COID = '{?COID_I}'
    AND CO.COMPANY_ID = '{?COMPANY_ID_I}'
    AND CO.COSEQ = {?COSEQ_I} 
    AND CO.COSUBSEQ = {?COSUBSEQ_I}
 GROUP BY
    PARTY. PARTY_ID        ,
    PARTY.NAME1,
    PARTY.ADR1,
    PARTY.ADR2,
    PARTY.PHONE,
    PARTY.POSTCODE,
    PARTY.CITY,
    PARTY.COUNTRYCODE,
    CO.COID,
    PBCAR.CONSIGNMENT_ID,
    PBROW.FPSHIPDTM,
    SUBSTR(CO.CO_REF, -4) ,
    SUBSTR(CO.CO_REF, -2) ,
    ART.ARTID,
    ART.ARTNAME2,
    PAK.PAKID,
    PAK.PAKNAME,
    CAR.CARTYPID,
    PBCAR.CARID,
    NVL(PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID),
    PBCAR.CONSOLIDATION_TO_CARID,
    CONSIGNMENT.FRE_CONSIGNMENT_ID,
    PBROW.COPOS,
    CO.DEPARTURE_ID,
    DECODE(PAK.PAKID, 'CAR',  20, PAK.WEIGHT),
    CAR.TOTVOL,
    CO.COSEQ,
    CO.COSUBSEQ
ORDER BY PBROW.COPOS