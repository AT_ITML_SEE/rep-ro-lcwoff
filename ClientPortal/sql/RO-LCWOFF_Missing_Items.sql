 /******************************************************************************
NAME: RO-LCWOFF_Missing_Items.rpt
SUBREPORT: 
DESC: Loaded departures
    
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
--------------------------------------------------------------------------------
1.0 		??.??.???? 		????????            	    created
1.1 		28.04.2022 		DAKORNHA     IMI-2116       Refactoring/Indexing

******************************************************************************/

/* Formatted on 28/04/2022 13:59:55 (QP5 v5.336) */
SELECT RCVROWLOG.ARTID,
    RCVROWLOG.ORDQTY,
    RCVROWLOG.CARID,
    RCVROWLOG.ASNINID,
    RCVROWLOG.ASNINROWID,
    RCVROWLOG.EXPARRDTM,
    RCVROWLOG.COMPANY_ID,
    RCVROWLOG.CREATEDTM,
    RCVROWLOG.RCVQTY
FROM RCVROWLOG
WHERE RCVROWLOG.RCVQTY = 0
    AND RCVROWLOG.WHID = 'BUK1'
    AND RCVROWLOG.COMPANY_ID = 'RO-LCWOFF'    
    AND RCVROWLOG.CREATEDTM 
        BETWEEN {?ARR_DATE_FROM}
        AND {?ARR_DATE_TO}
    /** optional parameters START **/
    AND DECODE('{?ASNID}', '', '1', RCVROWLOG.ASNINID) = DECODE('{?ASNID}', '', '1', '{?ASNID}')
    AND DECODE('{?CARID}', '', '1', RCVROWLOG.CARID) = DECODE('{?CARID}', '', '1', '{?CARID}')
    AND DECODE('{?ARTID}', '', '1', RCVROWLOG.ARTID) = DECODE('{?ARTID}', '', '1', '{?ARTID}')
    /** optional parameters END **/
ORDER BY RCVROWLOG.EXPARRDTM DESC,
         RCVROWLOG.ASNINID,
         RCVROWLOG.ASNINROWID