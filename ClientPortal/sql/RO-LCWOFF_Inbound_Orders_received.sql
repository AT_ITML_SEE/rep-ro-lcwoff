/******************************************************************************
NAME: RO-LCWOFF_Inbound_Orders_received.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

SELECT POID, 
    count(distinct POPOS) as "NUM_POS",
    TO_CHAR (CREATEDTM, 'yyyy-mm-dd HH24:mi') "CREATEDTM", 
    TO_CHAR (CREATEDTM, 'HH24:mi') AS "hour"
 FROM rcvrow
 WHERE     OWNID = 'RO-LCWOFF'
       AND TO_CHAR (CREATEDTM, 'yyyyMMdd HH24:MI:SS') >=
           TO_CHAR (SYSDATE - 1, 'yyyyMMdd') || ' 13:00:00'
       AND TO_CHAR (CREATEDTM, 'yyyyMMdd HH24:MI:SS') <=
           TO_CHAR (SYSDATE + 1, 'yyyyMMdd') || ' 13:00:00'
GROUP BY POID, 
    TO_CHAR (CREATEDTM, 'yyyy-mm-dd HH24:mi'), 
    TO_CHAR (CREATEDTM, 'HH24:mi')
ORDER BY POID asc