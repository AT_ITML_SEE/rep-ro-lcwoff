/******************************************************************************
NAME: RO-LCWOFF_Stock_Discr.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

SELECT 
    ITECHG.ITEID, 
    ITECHG.ARTID, 
    ITECHG.STORQTY, 
    ITECHG.STORQTY2,
    ITECHG.BLOCKCOD,
    (SELECT BLOCKCODNM FROM BLOCKCOD WHERE BLOCKCOD.BLOCKCOD = ITECHG.BLOCKCOD) BLOCKTXT,
    ITECHG.BLOCKCOD2, 
    (SELECT BLOCKCODNM FROM BLOCKCOD WHERE BLOCKCOD.BLOCKCOD = ITECHG.BLOCKCOD2) BLOCKTXT2,
    ITECHG.ITECHGCODID, 
    ITECHG.EMPID, 
    ITECHG.SENDSTAT, 
    ITECHG.INSERTDTM DTM,
    ITECHGCOD.ITECHGCODNAME,
    ITECHG.POID,
    ITECHG.ITECHGTXT,
    ITECHG.WPADR
--    ITECHG.WSID||' - '||ITECHG.WPADR
FROM 
    ITECHG,
    ITECHGCOD 
WHERE 
    ITECHG.ITECHGCODID=ITECHGCOD.ITECHGCODID AND
    ITECHG.COMPANY_ID='RO-LCWOFF'