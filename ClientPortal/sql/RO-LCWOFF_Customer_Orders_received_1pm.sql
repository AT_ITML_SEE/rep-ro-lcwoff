/******************************************************************************
NAME: RO-LCWOFF_Customer_Orders_received_1pm.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

select coid, insertdtm 
from 
    co 
where 
  TO_CHAR(insertdtm, 'yyyyMMdd HH24:MI:SS')  >=  TO_CHAR(SYSDATE-1, 'yyyyMMdd')||' 13:00:00' AND
    TO_CHAR(insertdtm, 'yyyyMMdd HH24:MI:SS')  <=  TO_CHAR(SYSDATE, 'yyyyMMdd')||' 13:00:00' and
    company_id='RO-LCWOFF'