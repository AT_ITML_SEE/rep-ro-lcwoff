/******************************************************************************
NAME: RO-LCWOFF_Customer_Orders_sent.rpt
DESC: Customer orders status
    
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
--------------------------------------------------------------------------------
1.0 		??.??.???? 		????????            	    created
1.1 		14.04.2022 		DAKORNHA     IMI-2116       Refactoring/Indexing

******************************************************************************/

SELECT 
    COWORK.COID, 
    COWORK.INSERTDTM,
    COWORK.ACKSHIPDATE,
    PARTY.NAME1,
    PARTY.POSTCODE,
    PARTY.CITY,
    COROW.ARTID,
    ART.ARTNAME1,
    COROW.ITEID,
    COROW.ORDQTY,
    COROW.PICKQTY,
    DECODE(COROW.RESTCOD,'MIS','Missing'
                        ,'N','Balance exists, notpickable'
                        ,'S','Out of stock'
                        ,'CR','Client Request'
                        ,'FS','Error at balance check'
                        ,'U','Agreed under delivery') AS RESTCOD,
    DEP.LOADFINISHDTM,
    DECODE(COWORK.COSTATID,'00','Registered'
                            ,'05','Departure Requested'
                            ,'10','Departure Connected'
                            ,'19','Departure Connection Failure'
                            ,'20','Pick Generated'
                            ,'28','Error at Balance Check'
                            ,'29','Pick Generation Failure'
                            ,'30','Pick Started'
                            ,'40','Pick Completed'
                            ,'50','Pick Finished'
                            ,'80','Pick Reported') AS STATUS
FROM DEP
JOIN COWORK
    ON COWORK.DEPARTURE_ID=DEP.DEPARTURE_ID 
    AND COWORK.COMPANY_ID='RO-LCWOFF'
JOIN COROW
    ON COROW.COMPANY_ID=COWORK.COMPANY_ID
    AND COROW.COID=COWORK.COID
JOIN PARTY
    ON PARTY.PARTY_ID = COWORK.SHIPTOPARTYID 
JOIN ART
    ON COROW.COMPANY_ID=ART.COMPANY_ID
    AND COROW.ARTID=ART.ARTID
WHERE DEP.WHID = 'BUK1'
    AND DEP.DEPARTURE_DTM 
        BETWEEN TRUNC({?DATE_FROM}) 
        AND TRUNC({?DATE_TO}+1) 
    AND DEP.LOADFINISHDTM 
        BETWEEN TRUNC({?DATE_FROM}) 
        AND TRUNC({?LOAD_DATE_TO}+1) 
UNION
SELECT
    COTRC.COID, 
    COTRC.INSERTDTM,
    COTRC.ACKSHIPDATE,
    COTRC.NAME1,
    COTRC.POSTCODE,
    COTRC.CITY,
    COROWTRC.ARTID,
    COROWTRC.ARTNAME1,
    COROWTRC.ITEID,
    COROWTRC.ORDQTY,
    COROWTRC.PICKQTY,
    DECODE(COROWTRC.RESTCOD,'MIS','Missing'
                            ,'N','Balance exists, notpickable'
                            ,'S','Out of stock'
                            ,'CR','Client Request'
                            ,'FS','Error at balance check'
                            ,'U','Agreed under delivery') AS RESTCOD,
    DEPLOG.LOADFINISHDTM,
    REPLACE('80','80','Pick Reported')
FROM COTRC
JOIN COROWTRC
    ON COROWTRC.COID=COTRC.COID
    AND COROWTRC.COSEQ=COTRC.COSEQ
    AND COROWTRC.COSUBSEQ=COTRC.COSUBSEQ
    AND COROWTRC.COMPANY_ID=COTRC.COMPANY_ID
JOIN DEPLOG
    ON DEPLOG.DEPARTURE_ID=COTRC.DEPARTURE_ID
WHERE COROWTRC.WHID = 'BUK1'
    AND COROWTRC.COMPANY_ID = 'RO-LCWOFF'
    AND DEPLOG.DEPARTURE_DTM 
        BETWEEN TRUNC({?DATE_FROM}) 
        AND TRUNC({?DATE_TO}+1) 
    AND DEPLOG.LOADFINISHDTM 
        BETWEEN TRUNC({?LOAD_DATE_FROM}) 
        AND TRUNC({?LOAD_DATE_TO}+1) 


