/******************************************************************************
NAME: RO-LCWOFF_View_Item.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

SELECT 
    OLACODNL.OLACODTXT ITESTAT,
   ITE.COMPANY_ID,
   ITE.ITEID,
   ITE.ARTID,
   ITE.ARTNAME,
   ARTGRP.ARTGRPNAME,
   ITE.PAKID,
   ITE.STORQTY,
   CAR.WHID,
   CAR.WSID,
   CAR.WPADR,
   WS.WSNAME,
   ITE.ARRDATE,
   ITE.PRODLOT,
   ITE.STORBAT,
   ITE.OWNID,
   ITE.LAST_CONSUMING_DATE,
   ITE.SERNUMB,
   ITE.PROMOTN,
   BLOCKCOD.BLOCKCODNM,
   OLACODTXT,
   LAST_CONSUMING_DATE - TRUNC (SYSDATE) AS SHELFDAYS
FROM ITE,
   CAR,
   BLOCKCOD,
   ART,
   ARTGRP,
   OLACODNL,
   WS
WHERE     CAR.CARID = ITE.CARID
   AND BLOCKCOD.BLOCKCOD = ITE.BLOCKCOD
   AND ART.COMPANY_ID = ITE.COMPANY_ID
   AND ART.ARTID = ITE.ARTID
   AND ARTGRP.COMPANY_ID = ART.COMPANY_ID
   AND ARTGRP.ARTGROUP = ART.ARTGROUP
   AND OLACOD = ITESTAT
   AND OLAID = 'ITESTAT'
   AND NLANGCOD = 'ENU'
   AND WS.WHID = CAR.WHID
   AND WS.WSID = CAR.WSID
   AND ITE.COMPANY_ID = 'RO-LCWOFF'
   AND CAR.COMPANY_ID = 'RO-LCWOFF'