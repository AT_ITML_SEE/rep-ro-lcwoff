/******************************************************************************
NAME: RO-LCWOFF_Goods_Movement_Report.rpt
SUBREPORT: 
DESC: Goods movement
    
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
--------------------------------------------------------------------------------
1.0 		??.??.???? 		????????            	    created

******************************************************************************/

SELECT a.company_id,
          CASE
             WHEN e.NLANGCOD IN ('SV', 'SVE') THEN 'Inleverans'
             ELSE 'Inbound'
          END
             Transaction_type,
          A.ARTID Article_No,
          C.ARTNAME1,
          '' SENDTOMS,
          A.POID Identitet,
          A.ITEID Item_Load,
          A.DELQTY Quantity,
          A.ARRDTM Transaction_Date,
          '' Storage_Location,
          '' Transaction_Comment,
          a.prodlot,
          a.promotn,
          a.sernumb,
          a.LAST_CONSUMING_DATE,
          0 STORQTY,
          a.pakid,
          0 original_quantity,
          0 updated_quantity,
          SUPID PART,
          '' PARTNAME,
          A.BLOCKCOD,
          d.serial_number,
          a.ownid OWNID,
          '' OWNID_TO,
          a.proid user1
     FROM DELLOGROW A,
          ART C,
          COMPANY e,
          wws_serialnumber d
    WHERE     A.COMPANY_ID = e.COMPANY_ID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND a.artid = c.artid
          and a.company_id=d.company_id
          and a.artid=d.artid
          and a.iteid=d.iteid
          and e.company_id='RO-LCWOFF'
          and C.WWS_ENHANCED_SERIAL_IN=1
   UNION ALL
   SELECT a.company_id,
          CASE
             WHEN e.NLANGCOD IN ('SV', 'SVE') THEN 'Inleverans'
             ELSE 'Inbound'
          END
             Transaction_type,
          A.ARTID Article_No,
          C.ARTNAME1,
          '' SENDTOMS,
          A.POID Identitet,
          A.ITEID Item_Load,
          A.DELQTY Quantity,
          A.ARRDTM Transaction_Date,
          '' Storage_Location,
          '' Transaction_Comment,
          a.prodlot,
          a.promotn,
          a.sernumb,
          a.LAST_CONSUMING_DATE,
          0 STORQTY,
          a.pakid,
          0 original_quantity,
          0 updated_quantity,
          SUPID PART,
          '' PARTNAME,
          A.BLOCKCOD,
          d.serial_number,
          a.ownid OWNID,
          '' OWNID_TO,
          a.proid user1
     FROM DELLOGROW A,
          ART C,
          COMPANY e,
          wws_serialnumber_log d
    WHERE     A.COMPANY_ID = e.COMPANY_ID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND a.artid = c.artid
          and a.company_id=d.company_id
          and a.artid=d.artid
          and a.iteid=d.iteid
          and e.company_id='RO-LCWOFF'
          and C.WWS_ENHANCED_SERIAL_IN=1
UNION ALL
    SELECT a.company_id,
          CASE
             WHEN e.NLANGCOD IN ('SV', 'SVE') THEN 'Inleverans'
             ELSE 'Inbound'
          END
             Transaction_type,
          A.ARTID Article_No,
          C.ARTNAME1,
          '' SENDTOMS,
          A.POID Identitet,
          A.ITEID Item_Load,
          A.DELQTY Quantity,
          A.ARRDTM Transaction_Date,
          '' Storage_Location,
          '' Transaction_Comment,
          a.prodlot,
          a.promotn,
          a.sernumb,
          a.LAST_CONSUMING_DATE,
          0 STORQTY,
          a.pakid,
          0 original_quantity,
          0 updated_quantity,
          SUPID PART,
          '' PARTNAME,
          A.BLOCKCOD,
          '',
          a.ownid OWNID,
          '' OWNID_TO,
          a.proid user1
     FROM DELLOGROW A,
          ART C,
          COMPANY e
    WHERE     A.COMPANY_ID = e.COMPANY_ID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND a.artid = c.artid
          and e.company_id='RO-LCWOFF'
          and C.WWS_ENHANCED_SERIAL_IN=0
   UNION ALL
   SELECT a.company_id,
          CASE
             WHEN e.NLANGCOD IN ('SV', 'SVE') THEN 'Utleverans'
             ELSE 'Outbound'
          END,
          A.ARTID,
          C.ARTNAME1,
          '' SENDTOMS,
          A.COID || ' - ' || A.COSEQ,
          A.ITEID,
            CASE
               WHEN a.pakid = b.BASPAKID THEN a.pickqty
               ELSE a.pickqty * NVL (b.baseqty, 0)
            END
          * -1,
          A.PICKDTM,
          CASE
             WHEN A.WPADR2 IS NULL THEN A.WSID || ' ' || A.WPADR
             ELSE A.WSID || ' ' || A.WPADR2
          END,
          'Plockorder: ' || A.PBHEADID,
          a.prodlot,
          a.promotn,
          a.sernumb,
          a.EXPIRYDATE,
          CASE
             WHEN a.pakid = b.BASPAKID THEN a.pickqty
             ELSE a.pickqty * NVL (b.baseqty, 0)
          END
             "pickqty",
          b.BASPAKID PAKID,
          (a.pickqty) "original qty",
          0,
          a.SHIPTOPARTYID,
          a.SHIPTOPARTY_NAME1,
          a.BLOCKCOD,
          d.serial_number,
          a.ownid OWNID,
          '' OWNID_TO,
          a.EMPID user1
     FROM owuser.PBROWLOG A,
          owuser.ART C,
          owuser.PAK B,
          COMPANY e,
          wws_serialnumber d
    WHERE     A.COMPANY_ID = e.COMPANY_ID
          AND A.ARTID = C.ARTID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND A.ARTID = B.ARTID(+)
          AND A.COMPANY_ID = B.COMPANY_ID(+)
          AND a.pakid = b.pakid(+)
          and a.company_id=d.company_id
          and a.artid=d.artid
          and a.iteid=d.iteid
          and e.company_id='RO-LCWOFF'
          and C.WWS_ENHANCED_SERIAL_IN=1
UNION ALL
    SELECT a.company_id,
          CASE
             WHEN e.NLANGCOD IN ('SV', 'SVE') THEN 'Utleverans'
             ELSE 'Outbound'
          END,
          A.ARTID,
          C.ARTNAME1,
          '' SENDTOMS,
          A.COID || ' - ' || A.COSEQ,
          A.ITEID,
            CASE
               WHEN a.pakid = b.BASPAKID THEN a.pickqty
               ELSE a.pickqty * NVL (b.baseqty, 0)
            END
          * -1,
          A.PICKDTM,
          CASE
             WHEN A.WPADR2 IS NULL THEN A.WSID || ' ' || A.WPADR
             ELSE A.WSID || ' ' || A.WPADR2
          END,
          'Plockorder: ' || A.PBHEADID,
          a.prodlot,
          a.promotn,
          a.sernumb,
          a.EXPIRYDATE,
          CASE
             WHEN a.pakid = b.BASPAKID THEN a.pickqty
             ELSE a.pickqty * NVL (b.baseqty, 0)
          END
             "pickqty",
          b.BASPAKID PAKID,
          (a.pickqty) "original qty",
          0,
          a.SHIPTOPARTYID,
          a.SHIPTOPARTY_NAME1,
          a.BLOCKCOD,
          d.serial_number,
          a.ownid OWNID,
          '' OWNID_TO,
          a.EMPID user1
     FROM owuser.PBROWLOG A,
          owuser.ART C,
          owuser.PAK B,
          COMPANY e,
          wws_serialnumber_log d
    WHERE     A.COMPANY_ID = e.COMPANY_ID
          AND A.ARTID = C.ARTID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND A.ARTID = B.ARTID(+)
          AND A.COMPANY_ID = B.COMPANY_ID(+)
          AND a.pakid = b.pakid(+)
          and a.company_id=d.company_id
          and a.artid=d.artid
          and a.iteid=d.iteid
          and e.company_id='RO-LCWOFF'
          and C.WWS_ENHANCED_SERIAL_IN=1
   UNION ALL
   SELECT a.company_id,
          CASE
             WHEN e.NLANGCOD IN ('SV', 'SVE') THEN 'Utleverans'
             ELSE 'Outbound'
          END,
          A.ARTID,
          C.ARTNAME1,
          '' SENDTOMS,
          A.COID || ' - ' || A.COSEQ,
          A.ITEID,
            CASE
               WHEN a.pakid = b.BASPAKID THEN a.pickqty
               ELSE a.pickqty * NVL (b.baseqty, 0)
            END
          * -1,
          A.PICKDTM,
          CASE
             WHEN A.WPADR2 IS NULL THEN A.WSID || ' ' || A.WPADR
             ELSE A.WSID || ' ' || A.WPADR2
          END,
          'Plockorder: ' || A.PBHEADID,
          a.prodlot,
          a.promotn,
          a.sernumb,
          a.EXPIRYDATE,
          CASE
             WHEN a.pakid = b.BASPAKID THEN a.pickqty
             ELSE a.pickqty * NVL (b.baseqty, 0)
          END
             "pickqty",
          b.BASPAKID PAKID,
          (a.pickqty)  "original qty",
          0,
          a.SHIPTOPARTYID,
          a.SHIPTOPARTY_NAME1,
          a.BLOCKCOD,
          '',
          a.ownid OWNID,
          '' OWNID_TO,
          a.EMPID user1
     FROM owuser.PBROWLOG A,
          owuser.ART C,
          owuser.PAK B,
          COMPANY e
    WHERE     A.COMPANY_ID = e.COMPANY_ID
          AND A.ARTID = C.ARTID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND A.ARTID = B.ARTID(+)
          AND A.COMPANY_ID = B.COMPANY_ID(+)
          AND a.pakid = b.pakid(+)
          and e.company_id='RO-LCWOFF'
           and C.WWS_ENHANCED_SERIAL_IN=0
   UNION ALL
    SELECT a.company_id,
          CASE
             WHEN e.NLANGCOD IN ('SV', 'SVE') THEN 'Justering'
             ELSE 'Hold/Release'
          END,
          A.ARTID,
          C.ARTNAME1,
          D.SENDTOMS,
          A.ITECHGCODID,
          CASE WHEN A.ITEID = '-1' THEN '' ELSE A.ITEID END,
          NVL (A.STORQTY2, 0) - NVL (A.STORQTY, 0),
          A.UPDDTM,
          CASE
             WHEN A.PPKEY IS NULL THEN ''
             ELSE NVL (B.WSID || ' ' || B.WPADR, 'PP RADERAD')
          END,
          A.ITECHGTXT,
          a.prodlot,
          a.promotn,
          a.sernumb2,
          (SELECT LAST_CONSUMING_DATE
             FROM ITE
            WHERE COMPANY_ID = a.COMPANY_ID AND ITEID = a.ITEID
           UNION
           SELECT LAST_CONSUMING_DATE
             FROM ITETRC
            WHERE COMPANY_ID = a.COMPANY_ID AND ITEID = a.ITEID),
          NVL (a.storqty, 0),
          a.pakid,
          NVL (a.storqty, 0),
          NVL (a.storqty2, 0),
          a.OWNID,
          '',
          A.BLOCKCOD2,
          f.serial_number,
          a.ownid OWNID,
          A.OWNID2 OWNID_TO,
          A.EMPID user1
     FROM ITECHG A,
          ART C,
          PP B,
          ITECHGCOD D,
          COMPANY e,
          wws_serialnumber f
    WHERE     A.COMPANY_ID = e.COMPANY_ID
          AND A.PPKEY = B.PPKEY(+)
          AND A.ARTID = C.ARTID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND A.ITECHGCODID = D.ITECHGCODID
          and a.company_id=f.company_id
          and a.artid=f.artid
          and a.iteid=f.iteid
          and e.company_id='RO-LCWOFF'
          and C.WWS_ENHANCED_SERIAL_IN=1
 UNION ALL
  SELECT a.company_id,
          CASE
             WHEN e.NLANGCOD IN ('SV', 'SVE') THEN 'Justering'
             ELSE 'Hold/Release'
          END,
          A.ARTID,
          C.ARTNAME1,
          D.SENDTOMS,
          A.ITECHGCODID,
          CASE WHEN A.ITEID = '-1' THEN '' ELSE A.ITEID END,
          NVL (A.STORQTY2, 0) - NVL (A.STORQTY, 0),
          A.UPDDTM,
          CASE
             WHEN A.PPKEY IS NULL THEN ''
             ELSE NVL (B.WSID || ' ' || B.WPADR, 'PP RADERAD')
          END,
          A.ITECHGTXT,
          a.prodlot,
          a.promotn,
          a.sernumb2,
          (SELECT LAST_CONSUMING_DATE
             FROM ITE
            WHERE COMPANY_ID = a.COMPANY_ID AND ITEID = a.ITEID
           UNION
           SELECT LAST_CONSUMING_DATE
             FROM ITETRC
            WHERE COMPANY_ID = a.COMPANY_ID AND ITEID = a.ITEID),
          NVL (a.storqty, 0),
          a.pakid,
          NVL (a.storqty, 0),
          NVL (a.storqty2, 0),
          a.OWNID,
          '',
          A.BLOCKCOD2,
          f.serial_number,
          a.ownid OWNID,
          A.OWNID2 OWNID_TO,
          A.EMPID user1
     FROM ITECHG A,
          ART C,
          PP B,
          ITECHGCOD D,
          COMPANY e,
          wws_serialnumber_log f
    WHERE     A.COMPANY_ID = e.COMPANY_ID
          AND A.PPKEY = B.PPKEY(+)
          AND A.ARTID = C.ARTID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND A.ITECHGCODID = D.ITECHGCODID
          and a.company_id=f.company_id
          and a.artid=f.artid
          and a.iteid=f.iteid
          and e.company_id='RO-LCWOFF'
          and C.WWS_ENHANCED_SERIAL_IN=1
 UNION ALL
   SELECT a.company_id,
          CASE
             WHEN e.NLANGCOD IN ('SV', 'SVE') THEN 'Justering'
             ELSE 'Hold/Release'
          END,
          A.ARTID,
          C.ARTNAME1,
          D.SENDTOMS,
          A.ITECHGCODID,
          CASE WHEN A.ITEID = '-1' THEN '' ELSE A.ITEID END,
          NVL (A.STORQTY2, 0) - NVL (A.STORQTY, 0),
          A.UPDDTM,
          CASE
             WHEN A.PPKEY IS NULL THEN ''
             ELSE NVL (B.WSID || ' ' || B.WPADR, 'PP RADERAD')
          END,
          A.ITECHGTXT,
          a.prodlot,
          a.promotn,
          a.sernumb2,
          (SELECT LAST_CONSUMING_DATE
             FROM ITE
            WHERE COMPANY_ID = a.COMPANY_ID AND ITEID = a.ITEID
           UNION
           SELECT LAST_CONSUMING_DATE
             FROM ITETRC
            WHERE COMPANY_ID = a.COMPANY_ID AND ITEID = a.ITEID),
          NVL (a.storqty, 0),
          a.pakid,
          NVL (a.storqty, 0),
          NVL (a.storqty2, 0),
          a.OWNID,
          '',
          A.BLOCKCOD2,
          '',
          a.ownid OWNID,
          A.OWNID2 OWNID_TO,
          A.EMPID user1
     FROM ITECHG A,
          ART C,
          PP B,
          ITECHGCOD D,
          COMPANY e
    WHERE     A.COMPANY_ID = e.COMPANY_ID
          AND A.PPKEY = B.PPKEY(+)
          AND A.ARTID = C.ARTID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND A.ITECHGCODID = D.ITECHGCODID
          and e.company_id='RO-LCWOFF'
          and C.WWS_ENHANCED_SERIAL_IN=0
 UNION ALL
    SELECT a.company_id,
          'Put-away/Movement Transfer' Transaction_type,
          A.ARTID Article_No,
          C.ARTNAME1,
          '' SENDTOMS,
          b.POID Identitet,
          b.ITEID Item_Load,
          b.ORIGINAL_STORQTY Quantity,
          a.ENDDTM Transaction_Date,
          a.TOWPADR Storage_Location,
          '' Transaction_Comment,
          b.prodlot,
          b.promotn,
          b.sernumb,
          b.LAST_CONSUMING_DATE,
          0 STORQTY,
          b.pakid,
          0 original_quantity,
          0 updated_quantity,
          SUPID PART,
          '' PARTNAME,
          b.BLOCKCOD,
          '',
          c.ownid OWNID,
          '' OWNID_TO,
          a.empid user1          
     FROM TRPLOG A,
          ITE B,
          ART C,
          COMPANY e
    WHERE     A.COMPANY_ID = e.COMPANY_ID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND a.CARID = b.CARID
          AND b.artid = c.artid
          and e.company_id='RO-LCWOFF'
          and A.TRPTYPID in (4, 5)
          and a.TOWPADR like 'A%'
          and C.WWS_ENHANCED_SERIAL_IN=0


