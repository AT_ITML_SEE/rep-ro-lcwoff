/******************************************************************************
NAME: RO-LCWOFF_Loaded_Departures.rpt
SUBREPORT: 
DESC: Loaded departures
    
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
--------------------------------------------------------------------------------
1.0 		??.??.???? 		????????            	    created
1.1 		11.04.2022 		DAKORNHA     IMI-2116       Refactoring/Indexing

******************************************************************************/

SELECT DISTINCT DEP.LOADFINISHDTM                  LOADDATE,
                CONSIGNMENT.COID                          ORDERNO,
                CONSIGNMENT.SHIPTOPARTYID || '-' || CONSIGNMENT.SHIPTOPARTY_NAME1     CUSTOMER,
                DEP.LOADFINISH_EMPID               USERLOAD,
                DEP.DLVRYMETH_ID,
                DEP.DEPARTURE_ID
FROM DEPLOG DEP
JOIN CONSIGNMENTTRC CONSIGNMENT
    ON CONSIGNMENT.DEPARTURE_ID = DEP.DEPARTURE_ID
    AND CONSIGNMENT.COMPANY_ID = 'RO-LCWOFF'
WHERE DEP.WHID = 'BUK1'
    AND TRUNC(DEP.LOADFINISHDTM)
        BETWEEN TRUNC({?LOAD_DATE_FROM})
        AND TRUNC({?LOAD_DATE_TO})
