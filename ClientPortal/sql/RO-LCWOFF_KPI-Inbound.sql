/******************************************************************************
NAME: RO-LCWOFF_KPI-Inbound.rpt
SUBREPORT: 
DESC: KPI Inbound
    
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
--------------------------------------------------------------------------------
1.0 		??.??.???? 		????????            	    created
1.1 		27.04.2022 		DAKORNHA     IMI-2116       Refactoring/Indexing

******************************************************************************/

SELECT DISTINCT
    RH.ASNINID,
    RH.INSTRUCTIONS,
    DECODE(RH.RCVTYPE, 'A3', RH.NOTRPCARS, NULL) ORDQTY,
    DECODE(RH.RCVTYPE, 'A3', RH.RCVTRPCARS, NULL) ARRQTY,
    DECODE(RH.RCVTYPE, 'A1', SUM(RL.ORDQTY), NULL) PCSORDQTY,
    DECODE(RH.RCVTYPE, 'A1', SUM(RL.ARRQTY), NULL) PCSARRQTY,
    DECODE(RH.RCVTYPE, 'A1', SUM(RL.RCVQTY), NULL) PCSRCVQTY,
    DECODE(RH.RCVTYPE, 'A3', SUM(DLR.DELQTY), NULL) DELQTY,
    MIN(RL.CREATEDTM) CREATEDTM,
    CASE WHEN RL.PRODLOT IN ('ROID',
                               'ROIDO',
                               'ROIDT',
                               'ROIMHD',
                               'ROINS',
                               'ROYD',
                               'RODU')
        THEN DECODE(RH.RCVTYPE, 'A1', 'Return A1', 'A3', 'Return A3', RH.RCVTYPE)
        ELSE DECODE(RH.RCVTYPE, 'A1', 'ASN Inbound A1', 'A3', 'ASN Inbound A3', RH.RCVTYPE)
    END INBOUNDTYPE,
    MIN (DLR.ARRDTM) INBOUNDSTARTDATE,
    MAX (DLR.ARRDTM) INBOUNDFINISHDATE
FROM RCVHEADLOG RH 
JOIN RCVROWLOG RL
    ON RH.RCVHEADID = RL.RCVHEADID
    AND RH.ASNINID = RL.ASNINID
    AND RH.ASNINSEQ = RL.ASNINSEQ
    AND RH.COMPANY_ID = RL.COMPANY_ID
JOIN DELLOGROW DLR
    ON DLR.DELID = RL.DELID
    AND DLR.ARRDTM IS NOT NULL
WHERE RH.WHID = 'BUK1'
    AND RH.RCVTYPE IN ('A1', 'A3')
    AND RH.COMPANY_ID = 'RO-LCWOFF'
    AND RH.SUPID = 'RO-LCWOFF'
    AND RL.CREATEDTM between {?insertdtm from} and {?insertdtm to}
    AND RH.UPDDTM >= {?insertdtm from}                   
GROUP BY RH.ASNINID,
    RH.INSTRUCTIONS,
    RH.NOTRPCARS,
    RH.RCVTRPCARS,
    RL.PRODLOT,
    RH.RCVTYPE
ORDER BY 9 desc, 
    8 desc


