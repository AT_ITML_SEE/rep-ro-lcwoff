/******************************************************************************
NAME: RO-LCWOFF_Article_Master_Data.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

select 
    art.artid,
    art.artname1,
    art.artname2, 
    ean.barcodetype,
    decode (pak.pakid,'PCS',ean.eandun,NULL),
    art.artgroup,
    art.arttypid,
    art.baspakid,
    pak.pakid,
    pak.baseqty,
    pak.weight,
    pak.net_weight,
    pak.length,
    pak.width,
    pak.height,
    pak.volume,
    pak.price1,
    pak.price2,
    art.company_id,
    art.instructions
from 
    art, pak, ean
where 
    art.artid=ean.artid and
    art.company_id=pak.company_id and
    art.artid=pak.artid and
    art.local=0 and
    art.company_id='RO-LCWOFF'
group by
    art.artid,
    art.artname1,
    art.artname2, 
    ean.barcodetype,
    decode (pak.pakid,'PCS',ean.eandun,NULL),
    art.artgroup,
    art.arttypid,
    art.baspakid,
    pak.pakid,
    pak.baseqty,
    pak.weight,
    pak.net_weight,
    pak.length,
    pak.width,
    pak.height,
    pak.volume,
    pak.price1,
    pak.price2,
    art.company_id,
    art.instructions
order by
art.artid,
pak.baseqty