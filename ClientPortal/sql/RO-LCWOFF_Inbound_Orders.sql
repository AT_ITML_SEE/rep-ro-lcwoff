/******************************************************************************
NAME: RO-LCWOFF_Inbound_Orders.rpt
SUBREPORT: 
DESC: Inbound Orders
    
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
--------------------------------------------------------------------------------
1.0 		??.??.???? 		????????            	    created
1.1 		27.04.2022 		DAKORNHA     IMI-2116       Refactoring/Indexing

******************************************************************************/

SELECT CAR.CARID,
    CAR.CARTYPID,
    ITE.ARTID,
    ITE.ARTNAME,
    RCVROW.ORDQTY,
    RCVROW.RCVQTY,
    ITE.PAKID,
    ITE.POID,
    ITE.POPOS,
    ITE.ARRDATE,    
    DECODE(RCVROW.RCVROWSTAT, 00, 'Registered'
        ,10, 'Planned'
        ,30, 'Started'
        ,40, 'Received'
        ,45, 'Completed'
        ,50, 'Processed'
        ,90, 'Rejected'
        , 'NO STATUS') AS STATUS,
    ITE.COMPANY_ID,
    RCVROW.COMPANY_ID,
    CAR.COMPANY_ID
FROM RCVROW
JOIN ITE 
    ON ITE.RCVROWID = RCVROW.RCVROWID
JOIN CAR 
    ON CAR.CARID = ITE.CARID
    AND CAR.COMPANY_ID = ITE.COMPANY_ID
JOIN WS
    ON WS.WHID = CAR.WHID
    AND WS.WSID = CAR.WSID
    AND RCVROW.WHID = WS.WHID
    AND ITE.ARRDATE 
        BETWEEN {?DATE_FROM}
        AND {?DATE_TO} 
    /** optional parameters START **/
    AND DECODE('{?POID}', 'all', '1', '{?POID}') = DECODE('{?POID}', 'all', '1', ITE.POID)
    AND DECODE('{?ART}', 'all', '1', '{?ART}') = DECODE('{?ART}', 'all', '1', ITE.ARTID)
    /** optional parameters END **/
WHERE  RCVROW.COMPANY_ID = 'RO-LCWOFF'
UNION
SELECT CAR.CARID,
    CAR.CARTYPID,
    ITE.ARTID,
    ITE.ARTNAME,
    RCVROW.ORDQTY,
    RCVROW.RCVQTY,
    ITE.PAKID,
    ITE.POID,
    ITE.POPOS,
    ITE.ARRDATE,    
    DECODE(RCVROW.RCVROWSTAT, 00, 'Registered'
        ,10, 'Planned'
        ,30, 'Started'
        ,40, 'Received'
        ,45, 'Completed'
        ,50, 'Processed'
        ,90, 'Rejected'
        , 'NO STATUS') AS STATUS,
    ITE.COMPANY_ID,
    RCVROW.COMPANY_ID,
    CAR.COMPANY_ID
FROM CAR
JOIN ITE 
    ON ITE.CARID = CAR.CARID
JOIN RCVROWLOG RCVROW
    ON RCVROW.RCVROWID = ITE.RCVROWID
    AND RCVROW.ITEID_PRE = ITE.ITEID
JOIN WS
    ON WS.WSID = CAR.WSID
    AND WS.WHID = CAR.WHID
WHERE  CAR.COMPANY_ID = 'RO-LCWOFF'
    AND WS.WHID = 'BUK1'
    AND ITE.ARRDATE 
        BETWEEN {?DATE_FROM}
        AND {?DATE_TO} 
    /** optional parameters START **/
    AND DECODE('{?POID}', 'all', '1', '{?POID}') = DECODE('{?POID}', 'all', '1', ITE.POID)
    AND DECODE('{?ART}', 'all', '1', '{?ART}') = DECODE('{?ART}', 'all', '1', ITE.ARTID)
    /** optional parameters END **/
ORDER BY
    ARRDATE DESC