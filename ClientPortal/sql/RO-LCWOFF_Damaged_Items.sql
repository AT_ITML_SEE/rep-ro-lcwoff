/******************************************************************************
NAME: RO-LCWOFF_Damaged_Items.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

SELECT ITE.ARTID,
    ITE.ITEID,
    ITE.LAST_TOUCH_EMPID AS EMPID,
    ITE.LAST_TOUCH_DTM AS DTM,
    ITE.BLOCKCOD,
    ITE.STORQTY,
    ITE.POID,
    CAR.WSID||' - '||CAR.WPADR
FROM ITE
JOIN CAR
    ON CAR.CARID = ITE.CARID
WHERE ITE.COMPANY_ID='RO-LCWOFF'
    AND ITE.WHID = 'BUK1'
    AND ITE.BLOCKCOD = 'DAM'
    AND ITE.POID IS NOT NULL
ORDER BY 
    ARRDATE DESC