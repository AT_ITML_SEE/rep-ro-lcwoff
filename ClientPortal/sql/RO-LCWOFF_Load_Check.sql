/******************************************************************************
NAME: RO-LCWOFF_Load_Check.rpt
SUBREPORT: 
DESC: Load check
    
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
--------------------------------------------------------------------------------
1.0 		??.??.???? 		????????            	    created
1.1 		14.04.2022 		DAKORNHA     IMI-2116       Refactoring/Indexing
1.2 		02.05.2022 		DAKORNHA     IMI-2116       optional parameters

******************************************************************************/

SELECT DISTINCT
    PBCAR.DEPARTURE_ID,
    PBROW.COID ORDERNO,
    PARTY.PARTY_ID || '-' || PARTY.NAME1 CUSTOMER,
    PBCAR.CARID,
    CARTYP.CARNAME AS CARTYPID,
    DEP.LOADFINISHDTM LOADDATE
FROM PBROWLOG PBROW
INNER JOIN PBCARLOG PBCAR
    ON PBCAR.PBCARID = PBROW.PBCARID
INNER JOIN CARTRC CAR
    ON CAR.CARID = PBCAR.CARID 
INNER JOIN CARTYP
    ON CARTYP.CARTYPID = CAR.CARTYPID
INNER JOIN PARTY
    ON PARTY.PARTY_ID = PBROW.SHIPTOPARTYID 
    AND PARTY.COMPANY_ID = PBROW.COMPANY_ID
    AND PARTY.PARTY_QUALIFIER = 'CU'
INNER JOIN DEPLOG DEP
    ON DEP.DEPARTURE_ID = PBROW.DEPARTURE_ID 
WHERE PBROW.COMPANY_ID = 'RO-LCWOFF'
    AND DEP.WHID = 'BUK1'
    /** optional parameters START **/
    AND DECODE('{?DEP_ID}', '', '1', DEP.DEPARTURE_ID) = DECODE('{?DEP_ID}', '', '1', '{?DEP_ID}')
    AND DECODE('{?COID}', '', '1', PBROW.COID) = DECODE('{?COID}', '', '1', '{?COID}')
    /** optional parameters END **/
    AND TRUNC(DEP.LOADFINISHDTM)
        BETWEEN TRUNC({?LOAD_DATE_FROM})
        AND TRUNC({?LOAD_DATE_TO}+1)
ORDER BY DEP.LOADFINISHDTM DESC


