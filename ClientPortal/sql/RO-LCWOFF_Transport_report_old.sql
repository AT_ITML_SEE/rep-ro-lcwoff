/******************************************************************************
NAME: RO-LCWOFF_Transport_report_old.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

select 
    cowork.departure_id,
    cowork.coid,
    substr(co.co_ref,-4),
    Case when dep.departure_pickstat = 'O' then 'Open'
    when dep.departure_pickstat = 'R' then 'Ready'
    when dep.departure_pickstat = 'H' then 'Hold'
    else 'Open'   
    End Deppickstat,
    dep.departure_dtm,
    cowork.ACKSHIPDATE,
    cowork.shipdtm DeliveryDate,
    dep.truckid LicensePlate,
    '' ArrivalDate,
    '' TruckType,
    '' ArrivalDate,
    'DB Schenker' Pickupfrom , 
    '087015' PickupZipCode,
    'Bucharest' PickUpCity,
    'CTP Bucharest WEST 2' PickUpAdress,
    party.party_id,
    party.name1,
    party.postcode,
    party.city,
    party.adr1,
    party.phone,
    cowork.weight,
    (select count(distinct carid) from pbrow, pbcar where pbrow.pbcarid=pbcar.pbcarid and pbrow.whid=pbcar.whid and 
    pbrow.coid = cowork.coid and pbrow.pbrowstat > '3' and pbrow.whid='BUK1' and carid is not null) cartons
from
    cowork, 
    party,
    dep,
    co
where
    cowork.departure_id=dep.departure_id and
    cowork.shiptopartyid=party.party_id and
    cowork.company_id=party.company_id and
    co.coid=cowork.coid and
    party.party_qualifier = 'CU' and
    cowork.company_id='RO-LCWOFF' 
group by
    cowork.departure_id,
    cowork.coid,
    co.co_ref,
    dep.departure_pickstat,
    dep.departure_dtm,
    cowork.ACKSHIPDATE,
    cowork.shipdtm,
    dep.truckid ,
    'DB Schenker', 
    '087015',
    'Bucharest',
    'CTP Bucharest WEST 2',
    party.party_id,
    party.name1,
    party.postcode,
    party.city,
    party.adr1,
    party.phone,
    cowork.weight
ORDER by
    cowork.departure_id,
    cowork.coid,
    dep.departure_dtm,
    dep.departure_pickstat,
    party.party_id